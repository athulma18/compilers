
datatype 'a Tree = Null | Node of 'a Tree * 'a * 'a Tree;

fun inorder Null = [] |
    inorder (Node(l,m,r)) = inorder l @ [m] @ inorder r ;

fun rotate_anticlk (Node(A,x,Node(B,y,C))) = Node(Node(A,x,B),y,C)
  | rotate_anticlk x = x;

fun singleton x = Node(Null,x,Null);

datatype 'a option = NONE | SOME of 'a;

fun head (x::xs) = SOME(x) |
    head  x = NONE;

fun zip (x::xs) (y::ys) = (x,y) :: zip xs ys 
  | zip [] [] = [];   
(* 
head: a list -> first element
datatype 'a  option = NONE | SOME of 'a

zip list * list = list


*)

    	  
